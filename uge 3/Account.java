/**
 * Created by Rasmus on 18-11-2015.
 */
public interface Account {
    public boolean deposit(int amount);

    public boolean withdraw(int amount);

    public int getBalance();

    public void yearEnd();
}
