package FileHandler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

/**
 * Created by fhvilshoj on 25/11/15.
 */
public class CatFiles {

    public static void main(String[] args) {

        if(args.length < 2){
            System.out.println("You should provide at least a ");
        }

        System.out.println("Starting read");

        ArrayList<String> files = new ArrayList<String>();
        for (int i = 0; i < args.length - 1; i++) {
            System.out.println("File: " + args[i]);
            try {
                Reader reader = new FileReader(args[i]);
                String file = "";
                BufferedReader br = new BufferedReader(reader);
                String s;
                while ((s = br.readLine()) != null) {
                    file += s + "\n";
                }
                files.add(file);
                System.out.println("Content: " + file);

            } catch (IOException e) {
                System.out.println("Could not find file: " + args[i]);
            }
        }

        StringBuilder sb = new StringBuilder();
        for(String s : files){
            sb.append(s);
        }

        try {
            FileWriter fr = new FileWriter(args[args.length-1]);
            fr.write(sb.toString());
            fr.close();
        } catch (IOException e){
            System.out.println("Could not write to the file because of an IOException");
        } catch (SecurityException e){
            System.out.println("Could not write to the file because of a Security issue");
        }

    }
}
