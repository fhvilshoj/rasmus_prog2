/**
 * Created by Rasmus on 18-11-2015.
 */
public class AccountWithInterest implements Account {
    private int balance;

    public AccountWithInterest() {
        balance = 0;
    }

    public boolean deposit(int amount) throws IllegalArgumentException {
        if (amount < 0) throw new IllegalArgumentException();
        balance += amount;
        return true;
    }

    public boolean withdraw(int amount) throws IllegalArgumentException {
        if (amount < 0) throw new IllegalArgumentException();
        if (balance >= amount) {
            balance -= amount;
            return true;
        }
        return false;
    }


    public int getBalance() {
        return balance;
    }

    public void yearEnd() {

        balance = (int) (balance * 1.05);
    }

}
