/**
 * Created by Rasmus on 18-11-2015.
 */
public class FeeBasedAccount implements Account {
    private int balance;
    private int trans;

    public FeeBasedAccount() {
        balance = 0;
        trans = 0;
    }

    public boolean deposit(int amount) throws IllegalArgumentException {
        if (amount < 0) throw new IllegalArgumentException();
        balance += amount;
        trans++;
        return true;
    }

    public boolean withdraw(int amount) throws IllegalArgumentException {
        if (amount < 0) throw new IllegalArgumentException();
        balance -= amount;
        trans++;
        return true;


    }


    public int getBalance() {
        return balance;
    }

    public void yearEnd() {
        balance -= trans;
    }
}
