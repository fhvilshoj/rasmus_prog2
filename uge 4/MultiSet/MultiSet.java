package MultiSet;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by fhvilshoj on 25/11/15.
 */
public class MultiSet<E> extends AbstractCollection<E> {
    private HashMap<E, Integer> entries;

    public MultiSet() {
        entries = new HashMap<E, Integer>();
    }

    public MultiSet(Collection<? extends E> collection) {
        entries = new HashMap<E, Integer>();
        for (E e : collection) {
            add(e);
        }
    }

    @Override
    public boolean add(E e) {
        if (entries.containsKey(e)) {
            int ctr = entries.get(e);
            entries.put(e, ctr + 1);
            return true;
        }
        entries.put(e, 1);
        return false;
    }

    @Override
    public boolean remove(Object e) {
        if (entries.containsKey(e)) {
            int ctr = entries.get(e);
            if (ctr > 1) {
                entries.put((E) e, ctr--);
            } else {
                entries.remove(e);
            }
            return true;
        }
        return false;
    }

    public int getCount(E e){
        if(entries.containsKey(e)){
            return entries.get(e);
        }
        return 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private Iterator<E> keys = entries.keySet().iterator();
            private E currElement = keys.next();

            @Override
            public boolean hasNext() {
                if (keys.hasNext())
                    return true;
                else if (entries.get(currElement) > 0) {
                    return true;
                }
                return false;
            }

            @Override
            public E next() {
                int count = entries.get(currElement);
                if (count > 0) {
                    entries.put(currElement, count - 1);
                    return currElement;
                }
                currElement = keys.next();
                return currElement;
            }


            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public int size() {
        int result = 0;
        for (Map.Entry<E, Integer> e : entries.entrySet()) {
            result += e.getValue();
        }
        return result;
    }

    @Override
    public String toString() {
        String result = "MultiSet: ";
        for (Map.Entry<E, Integer> e : entries.entrySet()) {
            for (int i = 0; i < e.getValue(); i++) {
                result += e.getKey() + ", ";
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object o){
        if(o == null) return false;
        if(o.getClass() != getClass()) return false;
        MultiSet<E> other = (MultiSet<E>) o;
        for(Map.Entry<E, Integer> e : entries.entrySet()){
            if(e.getValue() != other.getCount(e.getKey())) return false;
        }
        return true;
    }
}
