package Card;

/**
 * Created by Rasmus on 25-11-2015.
 */
public class Card implements Comparable<Card> {
    public enum Suit{DIAMOND,CLUB,HEART,SPADE}
    public enum Rank{TWO,THREE,FOUR,FIVE,SIX,SEVEN,EIGHT,NINE,TEN,JACK,QUEEN,KING,ACE}
    private Suit s;
    private Rank r;

    public Card(Suit s,Rank r){
        this.s = s;
        this.r = r;
    }

    public String toString(){
        return r.name() + s.name();
    }

    public boolean equals(Object o){
        if(o == null) return false;
        if(o.getClass() != getClass()) return false;
        Card other = (Card) o;
        return (other.r == r && other.s == s);
    }

    public int hashCode(){
        return r.hashCode() + s.hashCode();
    }

    public int compareTo(Card c){
        if(c.r.equals(r)){
            return c.s.compareTo(s);
        }
        return c.r.compareTo(r);
    }

}
