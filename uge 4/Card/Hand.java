package Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by fhvilshoj on 25/11/15.
 */
public class Hand {

    private HashSet<Card> cards;

    public Hand() {
        cards = new HashSet<Card>();
    }

    public boolean add(Card c) {
        if (!cards.contains(c)) {
            cards.add(c);
            return true;
        }
        return false;
    }

    public boolean remove(Card c) {
        if (cards.contains(c)) {
            cards.remove(c);
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        for (Card c : cards) {
            hash += 52 * c.hashCode();
        }
        return hash;
    }

    @Override
    public String toString() {
        ArrayList<Card> cardArr = new ArrayList<Card>();
        Collections.sort(cardArr);
        String result = "Hand: ";
        for (Card c : cards) {
            result += c + ", ";
        }
        return result;
    }

    public boolean equals(Object o) {
        if (o == null) return false;
        if (o.getClass() != getClass()) return false;
        Hand other = (Hand) o;
        return other.hashCode() == hashCode();
    }

}
